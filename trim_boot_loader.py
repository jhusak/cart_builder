import os
import sys
infile=sys.argv[1] 
outfile=sys.argv[2]
insize=os.stat(infile).st_size

print('*** TRIMMING XEX FILE ***')
with open(infile, 'rb') as in_file:
    with open(outfile, 'wb') as out_file:
        out_file.write(in_file.read()[6:insize-6])
