import os
infile  = 'bank_'
inext   = '.bnk'
outfile = 'ready.bin'
banksize=8192
banks=128
empty=0;

print('*** BUILDING BINARY CART IMAGE ***')
with open(outfile, 'wb') as out_file:
    bank = 0;
    while bank < banks :
        filename = infile + str(bank) + inext
        if os.path.isfile(filename):
            print('* bank ' + str(bank) + ' from file');
            with open(filename, 'rb') as in_file:
                indata = in_file.read()
                out_file.write(bytearray(indata))
        else:
            #print('* bank ' + str(bank) + ' empty');
            empty += 1;
            outblock = [0xff] * banksize
            out_file.write(bytearray(outblock))
        bank += 1;

    print('*** Empty banks: '+str(empty))
