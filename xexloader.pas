program xexloader;
uses atari;
var src,dest,last:word;
    runad,initad:word;
    datatomove:boolean;
    ram: array [0..0] of byte absolute 0;
    banks: array [0..128] of byte absolute $D500;
    b,bank: byte;
    consol: byte absolute $D01f;
    GINTLK: byte absolute $03FA;  // @nodoc 
    TRIG3: byte absolute $D013;   // @nodoc 

const CART_OFF = $80;

procedure SrcForward;
begin
    inc(src);
    if src = $C000 then begin
        src := $A000;
        inc(bank);
        banks[bank] := 0;
    end;
end;

function getWord: word;
begin
    banks[bank] := 0;
    result := ram[src];
    SrcForward;
    result := result + (ram[src] shl 8);
    SrcForward;
end;

begin
    bank := 1;
    src := $A000;
    runad := 0;
    pause;
    GINTLK := TRIG3; 
    portb := $fe;
    nmien := 0;
    dmactl := 0;

    if consol and %100 = 0 then begin
    asm
        {
        lda #1
        sta $d580
        sta PORTB
		jmp ($fffc)
        };
    end;

    repeat 
        dest := getWord;
        last := getWord;
        if dest = $ffff then begin
            dest := last;
            last := getWord;
        end;
        
        if dest <> 0 then datatomove := true;
        
        if dest = $02E2 then begin // INIT Block
            //Writeln('INIT BLOCK');
            initad := getWord;
            datatomove := false;
            asm {
                LDA initad      
                STA init_block+1
                LDA initad+1
                STA init_block+2
init_block
                jsr 0            
            };
        end;

        if dest = $02E0 then begin // RUN Block
            runad := getWord;
            datatomove := false;
            dest := 0;
        end;
                
        if datatomove then begin
            repeat 
                banks[bank]:=0;
                b := ram[src];
//                colbk := (b and %1100) or $30;
                banks[CART_OFF]:=0;
                ram[dest] := b;
                SrcForward;
//                colbk := 0;
                Inc(dest);
            until dest > last;
        end;
    
    until dest = 0;
    GINTLK := TRIG3; 
    banks[CART_OFF] := 0;
    if runad <> 0 then asm { jmp (runad) };
    asm { rts };
end.
