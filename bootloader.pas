program bootloader;
uses atari;
var src: word absolute $f0;
    dest: word absolute $f2;
    last: word absolute $f4;
    ram: array [0..0] of byte absolute 0;

function getWord(addr:word): word;
begin
    result := ram[addr] + (ram[addr+1] shl 8);
end;

begin
    src := $A180;
    inc(src,2);
    dest := getWord(src);
    inc(src,2);
    last := getWord(src);
    inc(src,2);
    repeat 
        ram[dest] := ram[src];
        Inc(src);
        Inc(dest);
    until dest > last;
    asm { rts };
end.
