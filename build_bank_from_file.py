import sys
import os
infile = sys.argv[1]
bank = int(sys.argv[2])
outname = 'bank_'
outext = '.bnk'
insize  = os.stat(infile).st_size
outsize = 8192

with open(infile, 'rb') as in_file:

    while insize > 0 :
    
        print('*** BUILDING BLOCK '+ str(bank) +' ***')
        outfile = outname + str(bank) + outext

        with open(outfile, 'wb') as out_file:
            outblock = [0xff] * outsize
            indata = in_file.read(outsize)
            outblock[0:len(indata)] = indata
            out_file.write(bytearray(outblock))
            insize -= len(indata)
            bank += 1

