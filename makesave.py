import sys
import os

if len(sys.argv) < 2:
    print('too few arguments')
    exit()

savesnum =  int(sys.argv[1])
    
outfilename = 'save_#.sav'
outsize = 512
ACHIEVEMENTS_COUNT = 12 * 6 + 12

outblock = [0xff] * outsize
outblock[outsize-1] = ord('F')

off = 0;
for w in range(6):
    outblock[off:off+64] = [0] * 64
    outblock[off] = w
    off += 64;
for i in range(ACHIEVEMENTS_COUNT):
        outblock[off + i] = i & 1

outblock[off + ACHIEVEMENTS_COUNT] = 250;
      

for i in range(savesnum):
    fname = outfilename.replace('#',f'{i:03d}');
    with open(fname, 'wb') as out_file:
        out_file.write(bytearray(outblock))
