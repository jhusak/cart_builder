# cart_builder

Simple toolchain for building Atari MaxFlash cartridge images. 
Adapt paths, executables and just use it in your own project. Or not.

Requires bash, python, Mad-Pascal.
