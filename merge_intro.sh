#!/bin/bash
GAME_NAME=$1
INTRO_NAME=$2

RUNADDR=`./ataricom.exe start.xex | grep RUN: | awk '{print $2}'`
echo $RUNADDR
./ataricom.exe -b 1-8 -i 0x$RUNADDR $INTRO_NAME _.xex
cat _.xex $GAME_NAME > _$GAME_NAME
